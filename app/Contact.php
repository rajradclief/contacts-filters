<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Contact extends Eloquent
{
    use SoftDeletes;

    // mongodb connection used
    protected $connection = 'mongodb';

    // mongodb collection used
    protected $collection = 'contacts';

    protected $casts = [
        'registered' => 'boolean'
    ];

    // carbon dates and softdelete
    protected $dates = ['first_seen', 'last_seen', 'deleted_at'];

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['app_id', 'channel', 'registered', 'first_name', 'last_name', 'email', 'mobile', 'first_seen', 'last_seen', 'tag', 'last_assigned_to', 'note'];

    // relationship
    public function webchat() {
        return $this->embedsOne(WebchatContact::class);
    }

    public function facebook() {
        return $this->embedsOne(FacebookContact::class);
    }

}
