<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FacebookContact extends Eloquent
{
    // mongodb connection used
    protected $connection = 'mongodb';

    // mongodb collection used
    protected $collection = 'facebook_details';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['messenger_id', 'profile_pic', 'locale', 'time_zone', 'gender', 'is_payment_enabled', 'last_ad_referral'];

    // relationship
    public function contacts()
    {
        return $this->belongsTo(Contact::class);
    }
}
