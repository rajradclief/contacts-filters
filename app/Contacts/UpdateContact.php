<?php

namespace App\Contacts;
use Illuminate\Http\Request;
use App\Contact;
use App\WebchatContact;
use Carbon\Carbon;
use App\Contacts\Contacts;
use App\Contacts\ContactsValidation;

class UpdateContact 
{   
  public function generalUpdateContact($contactid,$userData)
   {
       $key = $userData['key'];

       $value = $userData['value'];

       $matchFields = [$key = $value];
 
       $this->validation = new ContactsValidation();

       $validator = $this->validation->generalContactValidation($matchFields);

       if ($validator->fails()) 
       {
       	 return $validator->errors();
       }
       
       if ($key == 'email' || $key == 'mobile') 
       {
         	 if(filter_var($value,FILTER_VALIDATE_EMAIL)|| is_numeric($value)) 
         	 {
           	 	$checkContact = $this->checkContact($contactid,$userData);

             	 	if (!is_null($checkContact)) 
             	 	{
                 	 		if($checkContact['_id'] == $contactid) 
                 	 		{
                 	 			$this->lastSeenUpdate($contactid);
                 	 		}
             	 	  
             	 	 return $this->mergeContact($contactid, $userData, $checkContact);	 
             	 	}
         	 }
       }
       
       elseif ($key =='tag' || $key = 'note') 
       {

         	\DB::collection('contacts')->where('_id',$contactid)->push($key,$value);
           
           return $this->lastSeenUpdate($contactid); 
       }

       elseif ($key == 'last_seen') 
       {
       	  $this->lastSeenUpdate($contactid);
       }

       $contact = Contact::find($contactid);

       $contact->$key = $value;

       $contact->last_seen = Carbon::now();

       $contact->save();

       return $contact;

   }

   public function lastSeenUpdate($contactid)
   {
     $contact = Contact::find($contactid);

     $contact->last_seen = Carbon::now();

     $contact->save();

     return $contact;
   }

   public function checkContact($contactid,$userData)
   {
      $key = $userData['key'];

      $value = $userData['value'];

      $contact = Contact::find($contactid);

      $appId = $contact['app_id'];

      $contact = Contact::where($key,$value)->where('app_id',$appId)->first();

      return $contact;
   }

   public function mergeContact($contactid, $userData, $checkContact)
   {
   	 $contact = Contact::find($contactid);

   	 switch ($contact['channel']) 
   	 {
   	 	case 'facebook':

   	 		 return $this->mergeFacebookContact($contactid,$userData,$checkContact);

   	 		break;
   	 	case 'webchat':
                return $this->mergeWebchatContact($contactid, $userData, $checkContact);
                break;

            default:
                return 'default merge';
                break;
   	 }
   }
   public function mergeFacebookContact($contactid,$userData,$checkContact)
   {
   	 $contact = Contact::find($contactid);

   	 $checkContactId = $checkContact->_id;

   	 if ($contact->first_seen->gte($checkContact->first_seen)) 
   	 {
   	 	      $existContact = Contact::find($checkContactId);
 	 	        $existContact->first_name   = $contact->first_name;
            $existContact->last_name    = $contact->last_name;
            $existContact->last_seen    = Carbon::now();
            $existContact->last_assigned_to = $contact->last_assigned_to;
            $existContact->facebook->messenger_id = $contact->facebook->messenger_id;
            $existContact->facebook->profile_pic  = $contact->facebook->profile_pic;
            $existContact->facebook->locale       = $contact->facebook->locale;
            $existContact->facebook->gender       = $contact->facebook->gender;
            $existContact->facebook->time_zone    = $contact->facebook->time_zone;
            $existContact->facebook->is_payment_enabled = $contact->facebook->is_payment_enabled;
            $existContact->facebook->last_ad_referral   = $contact->facebook->last_ad_referral;
            $existContact->facebook->save();
            $contact->delete();
            return $existContact;
   	 }

   	  else
   	  { 
   	 	      $existContact = Contact::find($checkContactId);
            $contact->last_seen             = Carbon::now();
            $contact->webchat->socket_id    = $existContact->webchat->socket_id;
            $contact->webchat->session_id   = $existContact->webchat->session_id;
            $contact->webchat->city         = $existContact->webchat->city;
            $contact->webchat->region       = $existContact->webchat->region;
            $contact->webchat->country      = $existContact->webchat->couuntry;
            $contact->webchat->language     = $existContact->webchat->language;
            $contact->webchat->browser      = $existContact->webchat->browser;
            $contact->webchat->ip           = $existContact->webchat->ip;
            $contact->webchat->os           = $existContact->webchat->os;
            $contact->webchat->save();
            $existContact->delete();
            return $contact;
   	  }

   	  return 'error in facebook merge contact!!';
   }

   public function mergeWebchatContact($userData, $checkContact)
   {
      return $userData;
   }

   public function webchatUpdateContact($contactid,$userData)
   {
       $key = $userData['key'];
       $value = $userData['value'];
       $matchFields = [$key = $value];
       $this->validation = new ContactsValidation();
       $validator = $this->validation->webchatContactValidation($matchFields);

       if ($validator->fails()) 
       {
         return $validator->errors();
       }
        $contact = Contact::find($contactid);
        $contact->webchat->{$key} = $value;
        $contact->last_seen = Carbon::now();
        $contact->webchat->save();
        return $contact;
   }

   public function facebookUpdateContact($contactid,$userData)
   {
      $key = $userData['key'];
      $value = $userData['value'];
      $matchFields = [$key = $value];
      $this->validation = new ContactsValidation();
      $validator = $this->validation->facebookContactValidation($matchFields);
      if ($validator->fails())
       {
        return $validator->errors();
      }
        $contact = Contact::find($contactid);
        $contact->facebook->{$key}=$value;
        $contact->last_seen = Carbon::now();
        $contact->facebook->save();
        return $contact;
   } 
 }

