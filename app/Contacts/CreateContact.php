<?php

namespace App\Contacts;

use Illuminate\Http\Request;

use App\Contact;

use App\WebchatContact;

use App\FacebookContact;

use App\Contacts\ContactsValidation;

use Carbon\Carbon;

use App\Contacts\UpdateContact;

use DB;

class CreateContact
{
	 private $generalContactFields = [
            'first_name',
            'last_name',
            'email',
            'mobile',
            'last_seen',
            'tag',
            'last_assigned_to',
            'note'
        ];
	 private $webchatContactFields = [
	            'socket_id',
	            'session_id',
	            'city',
	            'region',
	            'country',
	            'language',
	            'browser',
	            'ip_address',
	            'os'
	    ];
	  private $facebookContactFields = [
            'profile_pic',
            'locale',
            'gender',
            'time_zone',
            'is_payment_enabled',
            'last_ad_referral'
        ];
	public function __construct()
	{
       $this->validation = new ContactsValidation();
	}

    public function contactCreation($userData,$tag)
    {
    	$contactCreation =  Contact::create([

    		    'app_id'        => $userData['app_id'],
	            'channel'       => $userData['channel'],
	            'registered'    => false,
	            'first_name'    => $userData['first_name'],
	            'last_name'     => $userData['last_name'],
	            'email'         => '',
	            'mobile'        => '',
	            'language'      => [],
	            'first_seen'    => Carbon::now(),
	            'last_seen'     => Carbon::now(),
	            'tag'           => [$tag],
	            'last_assigned_to' => $userData['last_assigned_to'],
	            'note'          => [],

	            ]);

    	return $contactCreation;
    }

	public function webchatContact($userData)
	{
         $validator = $this->validation->webchatValidation($userData);

	          if ($validator->fails()) 
	          {
	            return $validator->errors();
	          }

        $contacts = $this->contactCreation($userData,'webchat');
	        	
        $webchat = $contacts->webchat()->create([

	        	'socket_id' => $userData['socket_id'],
	            'session_id' => $userData['session_id'],
	            'city'      => $userData['city'],
	            'region'    => $userData['region'],
	            'country'   => $userData['country'],
	            'browser'   => $userData['browser'],
	            'ip_address' => $userData['ip_address'],
	            'os'        => $userData['os'],

            ]);
        $facebook = $contacts->facebook()->create([

	        	'messenger_id'      => '',
	            'profile_pic'       => '',
	            'locale'            => '',
	            'gender'            => '',
	            'time_zone'         => '',
	            'is_payment_enabled'    => '',
	            'last_ad_referral'      => '',

            ]);

        return $contacts;
	}

	public function facebookContact($userData)
	{
        $validator = $this->validation->facebookValidation($userData);

        if ($validator->fails()) 
        {
        	return $validator->errors();
        }

        $contacts = $this->contactCreation($userData,'facebook');

        $facebook = $contacts->facebook()->create([

        	'messenger_id'      => $userData['messenger_id'],
            'profile_pic'       => $userData['profile_pic'],
            'locale'            => $userData['locale'],
            'gender'            => $userData['gender'],
            'time_zone'         => $userData['time_zone'],
            'is_payment_enabled'    => $userData['is_payment_enabled'],
            'last_ad_referral'      => $userData['last_ad_referral']

            ]);

         $webchat = $contacts->webchat()->create([
            'socket_id' => '',
            'session_id' => '',
            'city'      => '',
            'region'    => '',
            'country'   => '',
            'browser'   => '',
            'ip_address' => '',
            'os'        => ''
        ]);

         return $contacts;
	}

	public function createdcontactList()
	{
		$contacts = Contact::all();

		return $contacts;
	}

	public function singleContact($singlecontact)
	{
		$contact = Contact::findOrFail($singlecontact);

		if (!is_null($contact))
		{
			return $contact;
		}

		return "404 Not found'";		
	}

	public function getAppContacts($appid)
	{
		$contacts = Contact::where('app_id',$appid)->get();

		return $contacts;
	}

	public function deleteTags($userData,$contactid)
	{
			
			$key = $userData['key'];

			$value = $userData['value'];

			\DB::collection('contacts')->where('_id',$contactid)->pull($key,$value);

			$contact = Contact::find($contactid);

			if (!is_null($contact)) 
			{
				return $contact;
			}

			return '404 Not found';
	}

	public function updateExistingContact($userData,$contactid)
	{
           $key = $userData['key'];
          
           $this->updateFields = new UpdateContact();

           if (in_array($key, $this->generalContactFields)) 
           {
          	  return $this->updateFields->generalUpdateContact($contactid,$userData);
           }
           elseif (in_array($key,$this->webchatContactFields)) 
           {
           	 return $this->updateFields->webchatUpdateContact($contactid,$userData);
           }
           elseif (in_array($key, $this->facebookContactFields)) 
           {
           	 return $this->updateFields->facebookUpdateContact($contactid,$userData);
           }

       return '404 key not found';  
	}
}