<?php

namespace App\Contacts;
use Illuminate\Http\Request;
use App\Contact;
use App\WebchatContact;
use App\FacebookContact;
use Carbon\Carbon;
use Validator;

class ContactsValidation
{
	public function webchatValidation($userData)
	{
		$validator = Validator::make($userData,
           [
                  'first_name'    => 'required|string',
                  'last_name'     => 'required|string',
                  'last_assigned_to' => 'required|numeric',
                  'socket_id'     => 'required|numeric',
                  'session_id'    => 'required|numeric',
                  'city'          => 'present|string',
                  'region'        => 'present|string',
                  'country'       => 'present|string',
                  'language'      => 'present|string',
                  'browser'       => 'present|string',
                  'ip_address'    => 'present|ip',
                  'os'            => 'present|string'

            ]);
            
        return $validator; 
	}

      public function facebookValidation($userData)
      {
          $validator = Validator::make($userData, 
            [
                 'first_name'    => 'required|string',
                  'last_name'     => 'required|string',
                  'last_assigned_to' => 'required|numeric',
                  'messenger_id'  => 'required|numeric',
                  'profile_pic'   => 'present|url',
                  'gender'        => 'present|string',
                  'locale'        => 'present',
                  'time_zone'      => 'present|numeric',
                  'is_payment_enabled' => 'present',
                  'last_ad_referral' => 'present'

            ]);

        return $validator;
      }

      public function generalContactValidation($matchFields)
      {
         $validator = Validator::make($matchFields,
            [

                  'first_name'    => 'sometimes|string',
                  'last_name'     => 'sometimes|string',
                  'email'         => 'sometimes|email',
                  'mobile'        => 'sometimes|numeric|max:10',
                  'tag'           => 'sometimes|string',
                  'note'          => 'sometimes|string',
                  'last_assigned_to' => 'sometimes|numeric'

            ]);

         return $validator;
      }

      public function webchatContactValidation($matchFields)
      {
         $validator = \Validator::make($matchFields,
            [

            'socket_id'     => 'sometimes|numeric',
            'session_id'    => 'sometimes|numeric',
            'city'          => 'sometimes|string',
            'region'        => 'sometimes|string',
            'country'       => 'sometimes|string',
            'language'      => 'sometimes|string',
            'browser'       => 'sometimes|string',
            'ip_address'    => 'sometimes|ip',
            'os'            => 'sometimes|string'

            ]);

        return $validator;
      }

      public function  facebookContactValidation($matchFields)
      {
         $validator = \Validator::make($matchFields,
          [
            'profile_pic'   => 'sometimes|url',
            'gender'        => 'sometimes|string',
            'time_zone'     => 'sometimes|numeric',

          ]);
        return $validator;
      }

}