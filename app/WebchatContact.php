<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class WebchatContact extends Eloquent
{
    // mongodb connection used
    protected $connection = 'mongodb';

    // mongodb collection used
    protected $collection = 'webchat_details';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['socket_id', 'session_id', 'city', 'region', 'country', 'language', 'browser', 'ip_address', 'os', 'unsubscribed_from_emails'];

    public function contacts()
    {
        return $this->belongsTo(Contact::class);
    }
}
