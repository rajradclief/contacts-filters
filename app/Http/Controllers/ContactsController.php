<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contact;
use App\webchatContact;
use App\FacebookContact;
use App\Contacts\CreateContact;
use App\Contacts\UpdateContact;
use Validator;
use DB;
use Carbon\Carbon;
class ContactsController extends Controller
{    
     private $generalFields = [
            'first_name',
            'last_name',
            'email',
            'mobile',
            'first_seen',
            'last_seen',
            'registered',
            'tag',
            'last_assigned_to',
            'note'
        ];
     private $webchatFields = [
                'socket_id',
                'session_id',
                'city',
                'region',
                'country',
                'language',
                'browser',
                'ip_address',
                'os'
        ];
      private $facebookFields = [
            'profile_pic',
            'locale',
            'gender',
            'time_zone',
            'is_payment_enabled',
            'last_ad_referral'
        ];
      
    public function __construct()
    {

        $this->contact = new CreateContact();
    }

    public function create(Request $request)
    {
        $userData = $request->all();       
        $validator = Validator::make($request->all(),[
            'app_id'=>'required|bail',
            'channel'=>'required|bail',
            ]);
        if($validator->fails()) {
            return $validator->errors();
        }
        switch ($userData['channel']) {
            case 'webchat':
                 return $this->contact->webchatContact($userData);
                break;
            
             case 'facebook':
                return $this->contact->facebookContact($userData);
                break;
            
            default:
                return 'Wrong channel buddy!';
                break;
        }
    }

    public function getContactList()
    {
      $contactList = $this->contact->createdcontactList();

      return $contactList;

    }

    public function singleContactList($singlecontact)
    {
    	$singleContactList = $this->contact->singleContact($singlecontact);

    	return $singleContactList;

    }

    public function appContactsList($appid)
    {
    	$appContact = $this->contact->getAppContacts($appid);

    	return $appContact;
    }

    public function deleteTagOrNote(Request $request,$contactid)
    {
          $userData = $request->all();

          $deleteTags = $this->contact->deleteTags($userData,$contactid);

          return $deleteTags;
    }

    public function updateContact(Request $request,$contactid)
    {
        $userData = $request->all();

        $existingContact = $this->contact->updateExistingContact($userData,$contactid);

        return $existingContact;
    }

    public function addingFilters($appid, Request $request)
    {
        $userData = $request->all();
        $key = $userData['key'];
        if (in_array($key, $this->generalFields))
            {
            return $this->generalContactFields($appid, $userData);
            }
        elseif (in_array($key, $this->webchatFields))
            {
            return $this->webchatContactFields($appid, $userData);
            }
        elseif (in_array($key, $this->facebookFields))
            {
            return $this->facebookContactFields($appid, $userData);
            }
        else
            {
            return $this->channelFields($appid, $userData);
            }
    }

     public function generalContactFields($appid,$userData)
    {
            $contact = $this->filters($appid,$userData,'');
      
            $key = $userData['key'];

            $value = $userData['value'];

            $filter = $userData['filter']; 

            $allowedKeys = ['first_seen', 'last_seen'];

            if (in_array($key, $allowedKeys) && $filter == 'after')
                {
                $carbon = new Carbon();
                $value = $carbon->createFromFormat('Y-m-d', $value)->addDay(1)->setTime(0, 0, 0);
                $contact = $contact->where($key, '>=', $value)->get();
                return $contact = json_decode($contact, true);
                }

            elseif(in_array($key, $allowedKeys) && $filter == 'before')
                {
                $carbon = new Carbon();
                $value = $carbon->createFromFormat('Y-m-d', $value)->addDay(1)->setTime(0, 0, 0);
                $contact = $contact->where($key, '<', $value)->get();
                return $contact = json_decode($contact, true);
                }

            elseif(in_array($key, $allowedKeys) && $filter == 'on')
                {
                $carbon = new Carbon();
                $date = $carbon->createFromFormat('Y-m-d', $value)->setTime(0, 0, 0);
                $nextDay = $carbon->createFromFormat('Y-m-d', $value)->addDay(1)->setTime(0, 0, 0);
                $contact = $contact->where($key, '>=', $date)->where($key, '<', $nextDay)->get();
                return $contact = json_decode($contact, true);
                } 
            elseif($key == 'registered' && $filter == 'is') 
                {
                    $value = ($value == 'true') ? true: false;

                    $contact = Contact::where('app_id', $appid)->where($key,$value)->get();

                    return $contact = json_decode($contact,true);  
                }   

            return $contact = json_decode($contact->get(),true);             
    }

    public function webchatContactFields($appid,$userData)
    { 
        $contact = $this->filters($appid,$userData,'webchat.');

        return json_decode($contact->get(),true);
    }

    public function facebookContactFields($appid,$userData)
    {
            $contact =  $this->filters($appid,$userData,'facebook.');

            return json_decode($contact->get(),true);
    }

    public function filters($appid,$userData, $type = '')
    {
            $key = $userData['key'];

            $value = $userData['value'];

            $filter = $userData['filter'];

            $contact = Contact::where('app_id', $appid);

              switch ($filter)
                    {
                case 'is':
                    $contact = $contact->where($type . $key, $value);
                    break;

                case 'is not':
                    $contact = $contact->where($type . $key, '!=', $value);
                    break;

                case 'start with':
                    $contact = $contact->where($type . $key, 'like', $value . '%');
                    break;

                case 'start with':
                    $contact = $contact->where($type . $key, 'like', $value . '%');
                    break;

                case 'ends with':
                    $contact = $contact->where($type . $key, 'like', '%' . $value);
                    break;

                case 'contains':
                    $contact = $contact->where($type . $key, 'like', '%' . $value . '%');
                    break;

                case 'does not contain':
                    $contact = $contact->where($type . $key, $value, 'IS NULL');
                    break;

                    case 'unknown':
                    $contact = $contact->where($type.$key, '');
                    break;

                default:
                     dd("Invalid Condition");
                     break;
                    }

         return $contact;
    }
     
    public function channelFields($appid, $userData)
    {   
        $value = $userData['value'];

        $key = $userData['key'];
 
        $filter = $userData['filter'];

        $allowedKeys = ['webchat','facebook'];

        if (in_array($value,$allowedKeys) && $filter == 'is') 
        {

            $contact = Contact::where('app_id', $appid)->where($key, $value)->get();

            return $contact = json_decode($contact,true);
        }
    }

}






