<?php


Route::prefix('contact')->group(function(){

    Route::post('create', 'ContactsController@create');

    Route::post('{contactid}/update','ContactsController@updateContact');

    Route::get('contact-list','ContactsController@getContactList');

    Route::get('{singlecontact}','ContactsController@singleContactList');

    Route::get('app/{appid}','ContactsController@appContactsList');

    Route::post('{contactid}/tag/delete','ContactsController@deleteTagOrNote');

    Route::post('{contactid}/note/delete','ContactsController@deleteTagOrNote');

    Route::post('filter/{appid}','ContactsController@addingFilters');

});


